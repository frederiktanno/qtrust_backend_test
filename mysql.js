const mysql = require('mysql');
const config = require('./config');

/*
    Connection pooling is used so that the application can request for different connections
    rather than using the same connection by using mysql.createConnection. Pooling is useful
    to safeguard against the case where the MySQL server unexpectedly closed the connection.
    Connections in each pool are also automatically closed at the end of each query
 */
var pool = mysql.createPool({
    connectionLimit:100,
    host: config.HOST,
    user: config.USERNAME,
    port: config.MYSQL_PORT,
    password: config.PASSWORD,
    database: config.DBNAME
  });

var conn;

pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
        else{
            console.error(`error connecting to database: ${err}`);
        }
    }
    console.log(`successfully connected to mysql at port ${config.MYSQL_PORT}`);
    if (connection) connection.release()
    return
})
module.exports = pool
  
  /*module.exports = {
      getConnection: function(callback){
        pool.getConnection(function(err,connection){
            if (err){
              if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                  console.error('Database connection was closed.');
              }
              if (err.code === 'ER_CON_COUNT_ERROR') {
                  console.error('Database has too many connections.');
              }
              if (err.code === 'ECONNREFUSED') {
                  console.error('Database connection was refused.');
              }
              callback(err,null);
            }
            conn = connection;
            callback(null,connection);
        });
      },
      closeConnection: function(){
        conn.release();
      }
  }*/