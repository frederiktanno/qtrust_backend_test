//this file contains the main REST API service functionalities
//acts as the router middleware to be used in index.js

const router = require('express').Router();
const db = require("../mysql");
const restServices = require('../service/restServices');

    /*
        GET http://localhost:${config.PORT}/qtrust/cars
        This route returns an HTTP status 200 and a JSON representation of all cars in the database
        upon successful operation, or error 500 with a message "Server Error" in case of unexpected database error
     */
    router.get("/cars",(req,res)=>{
       restServices.getCars(db,function(err,result){
            if(err){
                console.error(err);
                return res.status(500).send("Server Error");
            }
            return res.json(result);
         })
    });

    /*
        GET http://localhost:${config.PORT}/qtrust/cars/:id
        The expected parameter is the correct representation of a car ID
        This route returns an HTTP status 200 and a JSON representation of the car with the given ID in the database
        upon successful operation,
        HTTP status 400 and message of "Invalid Parameter in the URL" in case of invalid ID type
        (i.e some random string or numbers)
        HTTP status 404 and message of "Car not found" in case of valid ID format but the database returns
        an empty result set
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.get("/cars/:id",(req,res,next)=>{
        var id = req.params.id;
        if(!id){
            next();
        }
        restServices.getCar(db,id,function(err,result){
            if(err == "invalid parameter"){                
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server error");
                }
                if(result==null || result.length === 0){
                    //in compliance with RFC 7231
                    return res.status(404).send("Car Not Found");
                    
                }
            }
            
            return res.json(result);
            
         })
    });

    /*
        POST http://localhost:${config.PORT}/qtrust/cars/
        This route returns an HTTP status 200 and a JSON object containing the ID of the inserted data
        upon successful operation,
        HTTP status 400 and message of "Invalid JSON Format" in case of misshapen JSON given in the request body
        expected format:
        {
            carName:string,
            manufacturer:string,
            regNumber:string,
            type:string,
            productionYear:integer
        }
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.post("/cars",(req,res)=>{
        var data = req.body;
        restServices.postCar(db,data,function(err,result){
            if(err == "invalid JSON"){
                console.log(err);
                return res.status(400).send("Invalid JSON Format");
            }
            if(err == "server Error"){
                return res.status(500).send("Unexpected Error");
            }
            else{
                return res.status(200).json({id:result.insertId});
            }
        });
    });

    /*
        PUT http://localhost:${config.PORT}/qtrust/cars/:id
        This route returns an HTTP status 200 and a message saying "Car Updated"
        upon successful operation,
        HTTP status 400 and message of "Invalid JSON Format" in case of misshapen JSON given in the request body
        expected format:
        {
            carName:string,
            manufacturer:string,
            regNumber:string,
            type:string,
            productionYear:integer
        }
        HTTP status 400 and message of "Invalid Parameter in the URL" in case of invalid ID type
        (i.e some random string or numbers)
        HTTP status 404 and message of "Car not found" in case of valid ID format but the car is not
        found in the database
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.put("/cars/:id",(req,res,next)=>{
        var data = req.body;
        var id = req.params.id;
        if(!id){
            console.log("here");
            next();
        }
        restServices.updateCar(db,data,id,function(err,result){
            if(err=="invalid JSON"){
                return res.status(400).send("Invalid JSON Format");
            }
            if(err == "invalid parameter"){
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server Error");
                }
                else if(result.changedRows < 1){
                    return res.status(404).send("Car Not Found");
                }
                else{
                    return res.status(200).send("Car Updated");
                }
                
            }
        });
    });

    /*
        DELETE http://localhost:${config.PORT}/qtrust/cars/:id
        The expected parameter is the correct representation of a car ID
        This route returns an HTTP status 200 and a message of "Car Deleted"
        upon successful operation,
        HTTP status 400 and message of "Invalid Parameter in the URL" in case of invalid ID type
        (i.e some random string or numbers)
        HTTP status 404 and message of "Car not found" in case of valid ID format but the car is not
        found in the database
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.delete("/cars/:id",(req,res,next)=>{
        var id = req.params.id;
        if(!id){
            console.log("here");
            next();
        }
        restServices.deleteCar(db,id,function(err,result){
            if(err == "invalid parameter"){
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server Error");
                }
                else if(result.affectedRows < 1){
                    return res.status(404).send("Car Not Found");
                }
                else{
                    return res.status(200).send("Car Deleted");
                }
            }
        });
            
    });


module.exports = {
    router
}