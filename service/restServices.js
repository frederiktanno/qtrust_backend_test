//this file contains the main business logic and logical operations of the API
const car = require('../model/CarModel').Car;
const assert = require('chai').assert;
const mysql = require('mysql');

const table = "cars";

const carName = "carName";
const manufacturer = "manufacturer";
const regNumber = "regNumber";
const type = "type";
const productionYear = "productionYear";

var exports = module.exports = {};

/*
    this function is invoked upon get all cars request from routesAPI.js
    It executes a query to the database to retrieve all data from the cars table
    in the database
    returns a callback with null error and the result set upon successful operation
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.getCars = function(db,callback){
    let selectQuery = "SELECT * FROM ??";
    let mysqlQuery = mysql.format(selectQuery,table);
    db.query(mysqlQuery,function(err,result,fields){
        if(err)return callback(err,null);
        callback(null,result);
    })
}
/*
    this function is invoked upon get book by ID request from routesAPI.js
    It executes a query to the database to retrieve the requested data from the cars table
    returns a callback with null error and the result set upon successful operation
    returns a callback with "invalid parameter" error and null result set 
    if the ID supplied is of invalid format
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.getCar= function(db,id,callback){
    let selectQuery = "SELECT * FROM ?? WHERE id = ?";
    let mysqlQuery = mysql.format(selectQuery,[table,id]);
    db.query(mysqlQuery,function(err,result,fields){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        if(result.length === 0)return callback(null,result);
        var res = result[0];
        callback(null,res);
    });
}

/*
    this function is invoked upon car insertion request from routesAPI.js
    It executes a query to the database to push the requested data to the cars table
    in the database

    the supplied data must first be asserted with the expected data format before
    pushing them to the database

    returns a callback with null error and the result set upon successful operation
    returns a callback with "invalid JSON" error and null result set 
    if the supplied data is invalid
    returns a callback with "server JSON" error and null result set 
    if meets an unexpected error
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.postCar = function(db,data,callback){
    car.carName = data.carName;
    car.manufacturer = data.manufacturer;
    car.regNumber = data.regNumber;
    car.type = data.type;
    car.productionYear = data.productionYear;
    try{
        assert.typeOf(data.carName,"string","expected carName to be string");
        assert.typeOf(data.manufacturer,"string","expected manufacturer to be string");
        assert.typeOf(data.regNumber,"string", "expected regNumber to be string");
        assert.typeOf(data.type,"string", "expected type to be string");
        assert.isNumber(data.productionYear,"expected productionYear to be Integer");
        assert.deepEqual(data,car);
    }
    catch(e){
        console.error(e);
        return callback("invalid JSON",null);

    }
    let insertQuery = "INSERT INTO ?? (??,??,??,??,??) VALUES (?,?,?,?,?)";
    let query = mysql.format(insertQuery,[table,carName,manufacturer,regNumber,
        type,productionYear,data.carName,data.manufacturer,data.regNumber,
        data.type,data.productionYear]);
    db.query(query,function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        callback(null,result);
    });
}

/*
    this function is invoked upon car update request from routesAPI.js
    It executes a query to the database to push the requested data to the cars table
    in the database
    
    the supplied data must first be asserted with the expected data format before
    pushing them to the database

    returns a callback with null error and the result set upon successful operation
    
    returns a callback with "invalid JSON" error and null result set 
    if the supplied data is invalid
    
    returns a callback with "invalid parameter" error and null result set 
    if the ID supplied is of invalid format
    
    returns a callback with "server error" error and null result set 
    if meets an unexpected error
    
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.updateCar = function(db,data,id,callback){
    delete car.id;
    car.carName = data.carName;
    car.manufacturer = data.manufacturer;
    car.regNumber = data.regNumber;
    car.type = data.type;
    car.productionYear = data.productionYear;
    try{
        assert.typeOf(data.carName,"string","expected carName to be string");
        assert.typeOf(data.manufacturer,"string","expected manufacturer to be string");
        assert.typeOf(data.regNumber,"string", "expected regNumber to be string");
        assert.typeOf(data.type,"string", "expected type to be string");
        assert.isNumber(data.productionYear,"expected productionYear to be Integer");
        assert.deepEqual(data,car);
    }
    catch(e){
        console.error(e);
        return callback("invalid JSON",null);

    }
    var updateQuery = "UPDATE ?? SET ?? = ?, ?? = ?, ?? = ?, ?? = ?, ?? = ? WHERE" 
    +" id=?";
    var query = mysql.format(updateQuery,[table,carName,data.carName,manufacturer,
        data.manufacturer,regNumber,data.regNumber,type,data.type,
        productionYear,
        data.productionYear,id])
    db.query(query,function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        callback(null,result);
    });
}
/*
    this function is invoked upon car deletion request from routesAPI.js
    It executes a query to the database to delete the requested car from the cars 
    table in the database

    returns a callback with null error and the result set upon successful operation
    
    returns a callback with "invalid parameter" error and null result set 
    if the ID supplied is of invalid format
    
    returns a callback with an error and null result set if encountered an error during query
    execution
 */

exports.deleteCar = function(db,id,callback){
    var deleteQuery = "DELETE FROM ?? WHERE id = ?";
    var query = mysql.format(deleteQuery,[table,id]);
    db.query(query,function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        callback(null,result);
    });
}
