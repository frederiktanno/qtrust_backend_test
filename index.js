/*
  Author: Laurensius Frederik Tanno
  Note: the table used for this application can be retrieved from cars.sql
 */

const app = require('express')();
const config = require('./config');
const port = config.PORT;
const routesAPI = require("./controller/routesAPI");
const bodyParser = require("body-parser");

app.use(bodyParser.json());

app.listen(port, () =>
  console.log(`Server listening on port ${port}!`)
);
//uses the router middleware
const router = routesAPI.router;
app.use("/qtrust/",router);